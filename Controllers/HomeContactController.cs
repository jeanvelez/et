﻿using ET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class HomeContactController : SurfaceController
	{
		[ChildActionOnly]
		public ActionResult ShowForm(string language)
		{
			HomeContactModel homeContactModel = new HomeContactModel();
			homeContactModel.Captcha = homeContactModel.generateCaptcha();
			List<SelectListItem> list = new List<SelectListItem>();
			int id = 1144;
			if (language.Substring(0, 2).ToString() == "en")
			{
				id = 1145;
			}
			IEnumerable<string> preValuesByDataTypeId = Umbraco.DataTypeService.GetPreValuesByDataTypeId(id);
			IEnumerator<string> enumerator = preValuesByDataTypeId.GetEnumerator();
			while (enumerator.MoveNext())
			{
				string text = enumerator.Current.ToString();
				list.Add(new SelectListItem
				{
					Text = text,
					Value = text
				});
			}
			homeContactModel.ListOfServiceOfInterest = list;
			List<SelectListItem> list2 = new List<SelectListItem>();
			IEnumerable<string> preValuesByDataTypeId2 = Umbraco.DataTypeService.GetPreValuesByDataTypeId(1143);
			IEnumerator<string> enumerator2 = preValuesByDataTypeId2.GetEnumerator();
			while (enumerator2.MoveNext())
			{
				string text2 = enumerator2.Current.ToString();
				list2.Add(new SelectListItem
				{
					Text = text2,
					Value = text2
				});
			}
			homeContactModel.ListOfFinancialInstitutions = list2;
			return PartialView("HomeContactForm", homeContactModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult CreateContactMessage(HomeContactModel contactMessage)
		{
			base.ViewBag.Contact = contactMessage;
			contactMessage.Captcha = base.TempData["homeContactCaptcha"].ToString();
			if (!base.ModelState.IsValid || contactMessage.CaptchaAnswer.ToString() != contactMessage.Captcha.ToString())
			{
				base.ModelState.AddModelError("Código", "Please enter the code exactly as seen on screen.");
				base.TempData["InvalidModelState"] = true;
				base.TempData.Keep("InvalidModelState");
				return CurrentUmbracoPage();
			}
			IContent content = base.Services.ContentService.CreateContent(contactMessage.Name + " - " + DateTime.Now.ToString(), 1882, "homeContactFormula");
			int comboId = 1144;
			if (contactMessage.language.ToString() == "en")
			{
				comboId = 1145;
			}
			IDataTypeService dataTypeService = ApplicationContext.Services.DataTypeService;
			IDataTypeDefinition dataTypeDefinition = dataTypeService.GetAllDataTypeDefinitions().First((IDataTypeDefinition x) => x.Id == comboId);
			if (contactMessage.SelectedServiceOfInterest != null)
			{
				int num = (from x in dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeDefinition.Id).PreValuesAsDictionary
						   where x.Value.Value == contactMessage.SelectedServiceOfInterest
						   select x.Value.Id).First();
				content.SetValue("serviceOfInterest", num);
			}
			IDataTypeDefinition dataTypeDefinition2 = dataTypeService.GetAllDataTypeDefinitions().First((IDataTypeDefinition x) => x.Id == 1143);
			if (contactMessage.SelectedFinancialInstitution != null)
			{
				int num2 = (from x in dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeDefinition2.Id).PreValuesAsDictionary
							where x.Value.Value == contactMessage.SelectedFinancialInstitution
							select x.Value.Id).First();
				content.SetValue("financialInstitution", num2);
			}
			content.SetValue("contactName", contactMessage.Name);
			content.SetValue("Email", contactMessage.Email);
			content.SetValue("Telephone", contactMessage.telephone);
			content.SetValue("Country", contactMessage.Country);
			content.SetValue("comments", contactMessage.Message);
			base.Services.ContentService.SaveAndPublishWithStatus(content);
			string str = "Nombre: " + contactMessage.Name + "\r\n";
			str = str + "Email: " + contactMessage.Email + "\r\n";
			str = str + "Telefono:" + contactMessage.telephone + "\r\n";
			str = str + "Pais:" + contactMessage.Country + "\r\n";
			str = str + "Servicio: " + contactMessage.SelectedServiceOfInterest + "\r\n";
			str = str + "Institucion Financiera:" + contactMessage.SelectedFinancialInstitution + "\r\n";
			str = str + "Mensaje: " + contactMessage.Message;
			MailMessage mailMessage = new MailMessage(contactMessage.Email, "marketing@evertecinc.com");
			mailMessage.Subject = "Email from Evertec Portal";
			mailMessage.Body = str;
			mailMessage.Bcc.Add("programmer@adworkscorp.com");
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = "smtp.gmail.com";
			smtpClient.Credentials = new NetworkCredential("adworks.emails@gmail.com", "S3nd3m*!l2018");
			smtpClient.Timeout = 300000;
			smtpClient.Port = 587;
			smtpClient.EnableSsl = true;
			smtpClient.Send(mailMessage);
			base.TempData["IsSuccessful"] = true;
			return Redirect(CurrentPage.Url + "#home-contact-form");
		}
	}
}
