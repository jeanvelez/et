﻿using ET.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class LandingController : SurfaceController
	{
		public ActionResult ShowForm()
		{
			LandingModel landingModel = new LandingModel();
			landingModel.WillAttend = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "yes",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "no",
					Value = 0,
					Selected = false
				}
			};
			landingModel.BringCompanion = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "yes",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "no",
					Value = 0,
					Selected = false
				}
			};
			return PartialView("Landing", landingModel);
		}

		[HttpPost]
		public ActionResult CreateLandingMessage(LandingModel landingMessage)
		{
			if (!base.ModelState.IsValid)
			{
				return CurrentUmbracoPage();
			}
			if (landingMessage.WillAttend[0].Selected && landingMessage.WillAttend[1].Selected)
			{
				base.ModelState.AddModelError("Will Attend", "Please select only one attendance option.");
				return CurrentUmbracoPage();
			}
			if (!landingMessage.WillAttend[0].Selected && !landingMessage.WillAttend[1].Selected)
			{
				base.ModelState.AddModelError("Will Attend", "Please select if you will attend.");
				return CurrentUmbracoPage();
			}
			if (landingMessage.BringCompanion[0].Selected && landingMessage.BringCompanion[1].Selected)
			{
				base.ModelState.AddModelError("Bring Companion", "Please select only one companion attendance option.");
				return CurrentUmbracoPage();
			}
			if (!landingMessage.BringCompanion[0].Selected && !landingMessage.BringCompanion[1].Selected)
			{
				base.ModelState.AddModelError("Bring Companion", "Please select if you will bring a companion.");
				return CurrentUmbracoPage();
			}
			base.ViewBag.Contact = landingMessage;
			string text = "";
			text = ((!landingMessage.WillAttend[0].Selected) ? landingMessage.WillAttend[1].Name : landingMessage.WillAttend[0].Name);
			string text2 = "";
			text2 = ((!landingMessage.BringCompanion[0].Selected) ? landingMessage.BringCompanion[1].Name : landingMessage.BringCompanion[0].Name);
			string str = "Name: " + landingMessage.Name + "\r\n";
			str = str + "Email: " + landingMessage.Email + "\r\n";
			str = str + "Will attend: " + text + "\r\n";
			str = str + "Will bring companion: " + text2 + "\r\n";
			MailMessage mailMessage = new MailMessage(landingMessage.Email, "mara.rivera@evertecinc.com");
			mailMessage.Subject = "Evertec NYC Style Form";
			mailMessage.Body = str;
			mailMessage.CC.Add("dserrano@evertecinc.com");
			mailMessage.Bcc.Add("programmer@adworkscorp.com");
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = "smtp.gmail.com";
			smtpClient.EnableSsl = true;
			smtpClient.Port = 587;
			smtpClient.Timeout = 300000;
			smtpClient.Credentials = new NetworkCredential("adworks.emails@gmail.com", "S3nd3m*!l2018");
			smtpClient.Send(mailMessage);
			base.TempData["IsSuccessful"] = true;
			base.TempData.Keep("IsSuccessful");
			base.ViewBag.IsSuccessful = true;
			return CurrentUmbracoPage();
		}
	}
}
