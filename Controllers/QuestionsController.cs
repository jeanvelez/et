﻿using ET.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class QuestionsController : SurfaceController
	{
		public ActionResult ShowForm()
		{
			QuestionsModel questionsModel = new QuestionsModel();
			questionsModel.WillAttend = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "Dear Evan Hansen",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "Hamilton",
					Value = 0,
					Selected = false
				}
			};
			questionsModel.BringCompanion = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "Yes",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "No",
					Value = 0,
					Selected = false
				}
			};
			questionsModel.UsOpenSession = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "Day Session",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "Night Session",
					Value = 2,
					Selected = false
				},
				new AttendOptions
				{
					Name = "Both**",
					Value = 3,
					Selected = false
				}
			};
			questionsModel.DietaryRequirements = new List<AttendOptions>
			{
				new AttendOptions
				{
					Name = "Yes",
					Value = 1,
					Selected = false
				},
				new AttendOptions
				{
					Name = "No",
					Value = 0,
					Selected = false
				}
			};
			return PartialView("Questions", questionsModel);
		}

		[HttpPost]
		public ActionResult CreateLandingMessage(QuestionsModel questionsMessage)
		{
			if (!base.ModelState.IsValid)
			{
				return CurrentUmbracoPage();
			}
			if (!questionsMessage.WillAttend[0].Selected && !questionsMessage.WillAttend[1].Selected)
			{
				base.ModelState.AddModelError("Will Attend", "Please select the Broadway play you will attend.");
				return CurrentUmbracoPage();
			}
			if (!questionsMessage.BringCompanion[0].Selected && !questionsMessage.BringCompanion[1].Selected)
			{
				base.ModelState.AddModelError("Bring Companion", "Please select if you will bring your companion to the NYSE visit.");
				return CurrentUmbracoPage();
			}
			if (!questionsMessage.UsOpenSession[0].Selected && !questionsMessage.UsOpenSession[1].Selected && !questionsMessage.UsOpenSession[2].Selected)
			{
				base.ModelState.AddModelError("Us Open Session", "Please select which US Open Session you will attend.");
				return CurrentUmbracoPage();
			}
			if (!questionsMessage.DietaryRequirements[0].Selected && !questionsMessage.DietaryRequirements[1].Selected)
			{
				base.ModelState.AddModelError("Dietary Requierements", "Please select if you have any dietary requirements.");
				return CurrentUmbracoPage();
			}
			if (questionsMessage.DietaryRequirements[0].Selected && string.IsNullOrEmpty(questionsMessage.DietaryRequirementsExplanation))
			{
				base.ModelState.AddModelError("Dietary Requierements Explanation", "You've selected that you have dietary requirements, please explain them so that we may accommodate you.");
				return CurrentUmbracoPage();
			}
			base.ViewBag.Contact = questionsMessage;
			string text = "";
			text = ((!questionsMessage.WillAttend[0].Selected) ? questionsMessage.WillAttend[1].Name : questionsMessage.WillAttend[0].Name);
			string text2 = "";
			text2 = ((!questionsMessage.BringCompanion[0].Selected) ? questionsMessage.BringCompanion[1].Name : questionsMessage.BringCompanion[0].Name);
			string text3 = "";
			text3 = (questionsMessage.UsOpenSession[0].Selected ? questionsMessage.UsOpenSession[0].Name : ((!questionsMessage.UsOpenSession[1].Selected) ? questionsMessage.UsOpenSession[2].Name : questionsMessage.UsOpenSession[1].Name));
			string text4 = "";
			text4 = ((!questionsMessage.DietaryRequirements[0].Selected) ? questionsMessage.DietaryRequirements[1].Name : questionsMessage.DietaryRequirements[0].Name);
			string str = "Name: " + questionsMessage.Name + "\r\n";
			str = str + "Email: " + questionsMessage.Email + "\r\n";
			str = str + "Which Broadway play do you and your companion prefer to attend? " + text + "\r\n";
			str = str + "Will you bring your companion to the NYSE visit on Friday afternoon? " + text2 + "\r\n";
			str = str + "Which US Open session do you and your companion prefer to attend? " + text3 + "\r\n";
			str = str + "Do you or your companion have any special dietary requirements? " + text4 + "\r\n";
			str = str + "(If yes, please explain): " + questionsMessage.DietaryRequirementsExplanation;
			MailMessage mailMessage = new MailMessage("mara.rivera@evertecinc.com", "mara.rivera@evertecinc.com");
			mailMessage.Subject = "Evertec NYC Questions Form";
			mailMessage.Body = str;
			mailMessage.CC.Add("dserrano@evertecinc.com");
			mailMessage.Bcc.Add("programmer@adworkscorp.com");
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = "smtp.gmail.com";
			smtpClient.EnableSsl = true;
			smtpClient.Port = 587;
			smtpClient.Timeout = 300000;
			smtpClient.Credentials = new NetworkCredential("adworks.emails@gmail.com", "S3nd3m*!l2018");
			smtpClient.Send(mailMessage);
			base.TempData["IsSuccessful"] = true;
			base.TempData.Keep("IsSuccessful");
			base.ViewBag.IsSuccessful = true;
			return CurrentUmbracoPage();
		}
	}
}
