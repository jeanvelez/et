﻿using ET.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class JobApplicationController : SurfaceController
	{
		public ActionResult ShowFormPR()
		{
			JobApplicationModel jobApplicationModel = new JobApplicationModel();
			jobApplicationModel.SelectedCountry = "Puerto Rico";
			jobApplicationModel.source = "Jobs";
			return PartialView("JobApplication", jobApplicationModel);
		}

		public ActionResult ShowForm()
		{
			JobApplicationModel jobApplicationModel = new JobApplicationModel();
			jobApplicationModel.ListOfCountries = getCountriesList();
			jobApplicationModel.source = "Jobs";
			return PartialView("JobApplication", jobApplicationModel);
		}

		public ActionResult ShowPFormPR()
		{
			JobApplicationModel jobApplicationModel = new JobApplicationModel();
			jobApplicationModel.SelectedCountry = "Puerto Rico";
			jobApplicationModel.source = "Programs";
			return PartialView("JobApplication", jobApplicationModel);
		}

		public ActionResult ShowPForm()
		{
			JobApplicationModel jobApplicationModel = new JobApplicationModel();
			jobApplicationModel.ListOfCountries = getCountriesList();
			jobApplicationModel.source = "Programs";
			return PartialView("JobApplication", jobApplicationModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult CreateContactMessage(JobApplicationModel contactMessage)
		{
			base.ViewBag.Contact = contactMessage;
			if (!base.ModelState.IsValid)
			{
				return CurrentUmbracoPage();
			}
			int num = 0;
			num = ((contactMessage.source == "Programs") ? ((!(contactMessage.SelectedCountry == "Puerto Rico")) ? 1512 : 1509) : ((!(contactMessage.SelectedCountry == "Puerto Rico")) ? 1427 : 1150));
			string text = string.Empty;
			if (contactMessage.FileName != string.Empty)
			{
				IMediaService mediaService = base.Services.MediaService;
				IMedia media = mediaService.CreateMedia(Path.GetFileName(contactMessage.FileName), 1492, "File");
				media.SetValue("umbracoFile", contactMessage.UploadedFile);
				mediaService.Save(media);
				text = media.Path;
			}
			IContent content = base.Services.ContentService.CreateContent(contactMessage.Name + " - " + DateTime.Now.ToString(), num, "JobApplicationFormula");
			int num2 = 0;
			if (contactMessage.SelectedCountry != "Puerto Rico")
			{
				IDataTypeService dataTypeService = ApplicationContext.Services.DataTypeService;
				IDataTypeDefinition dataTypeDefinition = dataTypeService.GetAllDataTypeDefinitions().First((IDataTypeDefinition x) => x.Id == 1484);
				num2 = (from x in dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeDefinition.Id).PreValuesAsDictionary
						where x.Value.Value == contactMessage.SelectedCountry
						select x.Value.Id).First();
			}
			if (text != string.Empty)
			{
				content.SetValue("uploadedFile", text);
			}
			content.SetValue("applicantCountry", num2);
			content.SetValue("applicantName", contactMessage.Name);
			content.SetValue("applicantEmail", contactMessage.Email);
			content.SetValue("applicantPhone", contactMessage.Telephone);
			base.Services.ContentService.SaveAndPublishWithStatus(content);
			string str = "Nombre: " + contactMessage.Name + "\r\n";
			str = str + "Email: " + contactMessage.Email + "\r\n";
			str = str + "Telefono: " + contactMessage.Telephone;
			str = str + "Pais: " + contactMessage.SelectedCountry + "\r\n";
			MailMessage mailMessage = new MailMessage(contactMessage.Email, "ian@adworkscorp.com");
			if (contactMessage.source == "Programs")
			{
				mailMessage.Subject = "Program Application - " + contactMessage.SelectedCountry + " from Evertec Website";
			}
			else
			{
				mailMessage.Subject = "Job Application - " + contactMessage.SelectedCountry + " from Evertec Website";
			}
			mailMessage.Body = str;
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = "mail.adworkscorp.com";
			smtpClient.Credentials = new NetworkCredential("programmer@adworkscorp.com", "Pr0gr*mm3r1");
			smtpClient.Send(mailMessage);
			base.TempData["IsSuccessful"] = true;
			return RedirectToCurrentUmbracoPage();
		}

		public List<SelectListItem> getCountriesList()
		{
			List<SelectListItem> list = new List<SelectListItem>();
			IEnumerable<string> preValuesByDataTypeId = Umbraco.DataTypeService.GetPreValuesByDataTypeId(1484);
			IEnumerator<string> enumerator = preValuesByDataTypeId.GetEnumerator();
			while (enumerator.MoveNext())
			{
				string text = enumerator.Current.ToString();
				list.Add(new SelectListItem
				{
					Text = text,
					Value = text
				});
			}
			return list;
		}
	}
}
