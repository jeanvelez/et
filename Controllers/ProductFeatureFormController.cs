﻿using ET.Models;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class ProductFeatureFormController : SurfaceController
	{
		public ActionResult ShowForm()
		{
			ProductFeatureFormModel model = new ProductFeatureFormModel();
			return PartialView("ProductFeatureForm", model);
		}

		[HttpPost]
		public ActionResult CreateMessage(ProductFeatureFormModel pfMessage)
		{
			if (!base.ModelState.IsValid)
			{
				return CurrentUmbracoPage();
			}
			base.ViewBag.Contact = pfMessage;
			string str = "Name: " + pfMessage.Name + "\r\n";
			str = str + "Last Name: " + pfMessage.LastName + "\r\n";
			str = str + "Email: " + pfMessage.Email + "\r\n";
			str = str + "Telephone: " + pfMessage.Telephone + "\r\n";
			str = str + "Financial Institution: " + pfMessage.FinancialInstitution + "\r\n";
			MailMessage mailMessage = new MailMessage(pfMessage.Email, "mara.rivera@evertecinc.com");
			mailMessage.Subject = "Evertec PVOT Form";
			mailMessage.Body = str;
			mailMessage.Bcc.Add("programmer@adworkscorp.com");
			SmtpClient smtpClient = new SmtpClient();
			smtpClient.Host = "smtp.gmail.com";
			smtpClient.EnableSsl = true;
			smtpClient.Port = 587;
			smtpClient.Timeout = 300000;
			smtpClient.Credentials = new NetworkCredential("adworks.emails@gmail.com", "S3nd3m*!l2018");
			smtpClient.Send(mailMessage);
			base.TempData["IsSuccessful"] = true;
			base.TempData.Keep("IsSuccessful");
			base.ViewBag.IsSuccessful = true;
			return CurrentUmbracoPage();
		}
	}
}
