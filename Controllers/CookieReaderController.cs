﻿using System;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class CookieReaderController : SurfaceController
	{
		[HttpPost]
		public bool SetDisplayNoticeToFalse()
		{
			HttpCookie httpCookie = new HttpCookie("__evt_cookie_notice");
			httpCookie.Values["cookieNotice"] = "false";
			httpCookie.HttpOnly = true;
			httpCookie.Secure = true;
			httpCookie.Expires = DateTime.Now.AddYears(5);
			base.ViewBag.cookieNotice = "false";
			base.Response.Cookies.Add(httpCookie);
			return true;
		}
	}
}
