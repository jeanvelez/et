﻿using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class CountryCheckboxGroupController : SurfaceController
	{
		public JsonResult GetCountries()
		{
			IPublishedContent content = Umbraco.TypedContent(1146);
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			foreach (IPublishedContent item in content.Children())
			{
				dictionary.Add(item.Name, item.Id);
			}
			return Json(dictionary, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetAppliedCountries(List<int> appliedIds)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
			if (appliedIds != null)
			{
				foreach (int appliedId in appliedIds)
				{
					if (!dictionary.ContainsValue(appliedId))
					{
						IPublishedContent publishedContent = umbracoHelper.TypedContent(appliedId);
						string text = "";
						text = ((publishedContent == null) ? "---" : publishedContent.Name);
						dictionary.Add(text, appliedId);
					}
				}
			}
			return Json(dictionary, JsonRequestBehavior.AllowGet);
		}
	}
}
