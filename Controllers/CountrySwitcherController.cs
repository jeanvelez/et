﻿using System;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace ET.Controllers
{
	public class CountrySwitcherController : SurfaceController
	{
		public ActionResult SwitchCountry(int countryId = 1184)
		{
			HttpCookie httpCookie = new HttpCookie("__evt_selected_country");
			dynamic val = Umbraco.Content(countryId);
			if (string.IsNullOrEmpty(val.Name))
			{
				val = Umbraco.Content(1184);
			}
			httpCookie.Values["countryId"] = val.Id.ToString();
			httpCookie.HttpOnly = true;
			httpCookie.Secure = true;
			httpCookie.Expires = DateTime.Now.AddYears(1);
			base.ViewBag.countryId = val.Id.ToString();
			base.Response.Cookies.Add(httpCookie);
			string url = "/es-pr/";
			object obj = val.link;
			if (val.link.HasValues)
			{
				dynamic val2 = val.link[0];
				if ((bool)val2.isInternal)
				{
					int id = val2["internal"];
					dynamic val3 = Umbraco.Content(id);
					url = val3.Url;
				}
				else
				{
					url = val.link[0].link;
				}
			}
			return Redirect(url);
		}
	}
}
