﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ET.Models
{
	public class QuestionsModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email address")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(200, MinimumLength = 2)]
		[Display(Name = "Name")]
		public string Name
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Which Broadway play do you and your companion prefer to attend?")]
		public List<AttendOptions> WillAttend
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Will you bring your companion to the NYSE visit on Friday afternoon?")]
		public List<AttendOptions> BringCompanion
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Which US Open session do you and your companion prefer to attend?")]
		public List<AttendOptions> UsOpenSession
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Do you or your companion have any special dietary requirements?")]
		public List<AttendOptions> DietaryRequirements
		{
			get;
			set;
		}

		[Display(Name = "(If yes, please explain)")]
		public string DietaryRequirementsExplanation
		{
			get;
			set;
		}
	}
}
