﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ET.Models
{
	public class ContactModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email address")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(200, MinimumLength = 2)]
		public string Name
		{
			get;
			set;
		}

		[StringLength(2500, MinimumLength = 0)]
		[DataType(DataType.MultilineText)]
		public string Message
		{
			get;
			set;
		}

		[DataType(DataType.PhoneNumber)]
		public string telephone
		{
			get;
			set;
		}

		public IEnumerable<SelectListItem> ListOfServiceOfInterest
		{
			get;
			set;
		}

		[Display(Name = "Service of Interest")]
		public string SelectedServiceOfInterest
		{
			get;
			set;
		}

		public IEnumerable<SelectListItem> ListOfFinancialInstitutions
		{
			get;
			set;
		}

		[Display(Name = "Financial Institution")]
		public string SelectedFinancialInstitution
		{
			get;
			set;
		}

		public string language
		{
			get;
			set;
		}
	}
}
