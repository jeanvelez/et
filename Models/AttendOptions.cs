﻿namespace ET.Models
{
	public class AttendOptions
	{
		public string Name
		{
			get;
			set;
		}

		public int Value
		{
			get;
			set;
		}

		public bool Selected
		{
			get;
			set;
		}
	}
}
