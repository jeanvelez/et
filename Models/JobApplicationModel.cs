﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace ET.Models
{
	public class JobApplicationModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email address")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(200, MinimumLength = 2)]
		public string Name
		{
			get;
			set;
		}

		[Required]
		[StringLength(50, MinimumLength = 10)]
		public string Telephone
		{
			get;
			set;
		}

		public IEnumerable<SelectListItem> ListOfCountries
		{
			get;
			set;
		}

		[Required]
		public string SelectedCountry
		{
			get;
			set;
		}

		public HttpPostedFileBase UploadedFile
		{
			get;
			set;
		}

		[FileExtensions(Extensions = "pdf", ErrorMessage = "Only PDF files are allowed.")]
		public string FileName
		{
			get
			{
				if (UploadedFile != null)
				{
					return UploadedFile.FileName;
				}
				return string.Empty;
			}
		}

		public string source
		{
			get;
			set;
		}
	}
}
