﻿using System.ComponentModel.DataAnnotations;

namespace ET.Models
{
	public class ProductFeatureFormModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email address")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, MinimumLength = 2)]
		[Display(Name = "Name")]
		public string Name
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, MinimumLength = 2)]
		[Display(Name = "Last Name")]
		public string LastName
		{
			get;
			set;
		}

		[Required]
		[Phone]
		[DataType(DataType.PhoneNumber)]
		[Display(Name = "Telephone")]
		public string Telephone
		{
			get;
			set;
		}

		[Required]
		[StringLength(100, MinimumLength = 2)]
		[Display(Name = "Financial Institution")]
		public string FinancialInstitution
		{
			get;
			set;
		}
	}
}
