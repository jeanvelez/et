﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ET.Models
{
	public class LandingModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Email address")]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(200, MinimumLength = 2)]
		[Display(Name = "Name")]
		public string Name
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Will Attend")]
		public List<AttendOptions> WillAttend
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Will you bring a companion")]
		public List<AttendOptions> BringCompanion
		{
			get;
			set;
		}

		public int selectedWillAttend
		{
			get;
			set;
		}
	}
}
