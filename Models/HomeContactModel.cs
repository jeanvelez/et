﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace ET.Models
{
	public class HomeContactModel
	{
		public int Id
		{
			get;
			set;
		}

		[Required]
		[EmailAddress]
		[DataType(DataType.EmailAddress)]
		public string Email
		{
			get;
			set;
		}

		[Required]
		[StringLength(200, MinimumLength = 2)]
		public string Name
		{
			get;
			set;
		}

		[StringLength(2500, MinimumLength = 0)]
		[DataType(DataType.MultilineText)]
		public string Message
		{
			get;
			set;
		}

		[Required]
		[DataType(DataType.PhoneNumber)]
		public string telephone
		{
			get;
			set;
		}

		[Required]
		[StringLength(50, MinimumLength = 2)]
		public string Country
		{
			get;
			set;
		}

		public IEnumerable<SelectListItem> ListOfServiceOfInterest
		{
			get;
			set;
		}

		[Display(Name = "Service of Interest")]
		public string SelectedServiceOfInterest
		{
			get;
			set;
		}

		public IEnumerable<SelectListItem> ListOfFinancialInstitutions
		{
			get;
			set;
		}

		[Display(Name = "Financial Institution")]
		public string SelectedFinancialInstitution
		{
			get;
			set;
		}

		public string language
		{
			get;
			set;
		}

		public string Captcha
		{
			get;
			set;
		}

		[Required]
		[Display(Name = "Código")]
		public string CaptchaAnswer
		{
			get;
			set;
		}

		public string generateCaptcha()
		{
			Random random = new Random();
			return new string((from s in Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890", 4)
							   select s[random.Next(s.Length)]).ToArray());
		}
	}
}
