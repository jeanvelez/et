﻿using System;
using System.Web;

namespace ET.Components
{
	public class CookieReader
	{
		public static int defaultCountryId => 1184;

		public static bool displayMap
		{
			get;
			set;
		}

		public static bool displayCookieNotice
		{
			get;
			set;
		}

		public static int GetCookie()
		{
			displayMap = false;
			displayCookieNotice = true;
			HttpCookie httpCookie = HttpContext.Current.Request.Cookies["__evt_selected_country"];
			HttpCookie httpCookie2 = HttpContext.Current.Request.Cookies["__evt_cookie_notice"];
			if (httpCookie2 == null || string.IsNullOrEmpty(httpCookie2.Values["cookieNotice"]))
			{
				displayCookieNotice = true;
				httpCookie2 = new HttpCookie("__evt_cookie_notice");
				httpCookie2.Values.Add("cookieNotice", "true");
				httpCookie2.Expires = DateTime.Now.AddYears(1);
				httpCookie2.Secure = true;
				httpCookie2.HttpOnly = true;
				HttpContext.Current.Response.Cookies.Add(httpCookie2);
			}
			else if (httpCookie2.Values["cookieNotice"] == "false")
			{
				displayCookieNotice = false;
			}
			int result;
			if (httpCookie == null || string.IsNullOrEmpty(httpCookie.Values["countryId"]))
			{
				result = defaultCountryId;
				httpCookie = new HttpCookie("__evt_selected_country");
				httpCookie.Values.Add("countryId", result.ToString());
				httpCookie.Expires = DateTime.Now.AddYears(1);
				httpCookie.Secure = true;
				httpCookie.HttpOnly = true;
				HttpContext.Current.Response.Cookies.Add(httpCookie);
				displayMap = true;
			}
			else if (!int.TryParse(httpCookie.Values["countryId"], out result))
			{
				result = defaultCountryId;
				displayMap = true;
			}
			return result;
		}
	}
}
